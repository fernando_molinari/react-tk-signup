# react-tk-signup

> reusable signup package for tk

[![NPM](https://img.shields.io/npm/v/react-tk-signup.svg)](https://www.npmjs.com/package/react-tk-signup) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-tk-signup
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-tk-signup'
import 'react-tk-signup/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [fernando_molinari](https://github.com/fernando_molinari)
