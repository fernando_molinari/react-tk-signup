import React from "react";
import SignupContent from "react-tk-signup";
import "react-tk-signup/dist/index.css";

const App = () => {
  const [suBtnEnabled, setSuBtnEnabled] = React.useState(true);
  const onSignupClick = (payload) => {
    setSuBtnEnabled(!suBtnEnabled);
  };

  return (
    <div className="App">
      <SignupContent
        onSignupClick={onSignupClick}
        schoolName=""
        signupBtnEnabled={suBtnEnabled}
        signupText="Please create your account."
        headerText="Thalamus Knowledge"
      />
    </div>
  );
};

export default App;
