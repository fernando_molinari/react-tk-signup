/*
 * Copyright 2021 by - - Fernando Molinari.
 * All rights reserved.
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import clsx from 'clsx';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const SignupContent = (props) => {
  const { schoolName, signupBtnEnabled, signupText, headerText, t } = props;

  const useStyles = makeStyles((theme) => ({
    paper: {
      margin: headerText ? theme.spacing(2, 4) : theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
      width: '35%',
    },
    btnWrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
    },
    margin: {
      margin: theme.spacing(2, 0, 1, 0),
    },
    marginConfirm: {
      margin: theme.spacing(0, 0, 1, 0),
    },
    textField: {
      width: '100%',
    },
    paragraph: {
      fontSize: '0.8rem',
      marginTop: '20px',
    },
    headerText: {
      marginTop: '0',
      marginBottom: '10px',
    },
  }));

  const ValidateEmail = (email) => {
    if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return true;
    }
    return false;
  };

  let sName = '';

  if (schoolName) {
    sName = `/${schoolName}`;
  }

  const classes = useStyles();

  const [btnDisabled, setBtnDisabled] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);

  // click events
  const [firstName, setFirstName] = useState(null);
  const [lastName, setLastName] = useState(null);
  const [userName, setUserName] = useState(null);
  const [password, setPassword] = useState(null);
  const [confirmPassword, setConfirmPassword] = useState(null);

  // validation
  const [isFirstNameError, setIsFirstNameError] = useState(false);
  const [isLastNameError, setIsLastNameError] = useState(false);
  const [isUserNameError, setIsUserNameError] = useState(false);

  const [isFirstNameHelperText, setIsFirstNameHelperText] = useState('');
  const [isLastNameHelperText, setIsLastNameHelperText] = useState('');
  const [isUserNameHelperText, setIsUserNameHelperText] = useState('');
  const [isPasswordHelperText, setIsPasswordHelperText] = useState('');
  const [
    isConfirmPasswordHelperText,
    setIsConfirmPasswordHelperText,
  ] = useState('');
  // validation

  const onHandleFirstName = (e) => {
    setIsFirstNameError(false);
    setIsFirstNameHelperText('');
    setFirstName(e.target.value.trim());
  };

  const onHandleLastName = (e) => {
    setIsLastNameError(false);
    setIsLastNameHelperText('');
    setLastName(e.target.value.trim());
  };

  const onHandleUserName = (e) => {
    setIsUserNameError(false);
    setIsUserNameHelperText('');
    setUserName(e.target.value.trim());
  };

  const onHandlePassword = (e) => {
    setIsPasswordHelperText('');
    setPassword(e.target.value.trim());
  };

  const onHandleConfirmPassword = (e) => {
    setIsConfirmPasswordHelperText('');
    setConfirmPassword(e.target.value.trim());
  };
  // click events

  // show/hide password
  const [values, setValues] = useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
  });

  const [valuesConfirm, setValuesConfirm] = useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleClickShowPasswordConfirm = () => {
    setValuesConfirm({
      ...valuesConfirm,
      showPassword: !valuesConfirm.showPassword,
    });
  };

  const handleMouseDownPasswordConfirm = (event) => {
    event.preventDefault();
  };
  // show/hide password

  // handle submit
  const onSignupClick = (event) => {
    event.preventDefault();

    setIsFirstNameError(false);
    setIsLastNameError(false);
    setIsUserNameError(false);

    if (!firstName) {
      setIsFirstNameError(true);
      setIsFirstNameHelperText(
        t ? t('typeFirstName') : 'Please type in your First Name',
      );
      return false;
    }

    if (!lastName) {
      setIsLastNameError(true);
      setIsLastNameHelperText(
        t ? t('typeLastName') : 'Please type in your Last Name',
      );
      return false;
    }

    if (!userName) {
      setIsUserNameError(true);
      setIsUserNameHelperText(
        t ? t('typeUserName') : 'Please type in your User Name',
      );
      return false;
    }

    // email wrong format
    if (!ValidateEmail(userName)) {
      setIsUserNameError(true);
      setIsUserNameHelperText(
        t ? t('typeValidEmail') : 'Please type in a valid email',
      );
      return false;
    }

    if (!password) {
      setIsPasswordHelperText(
        t ? t('typePassword') : 'Please type in your Password',
      );
      return false;
    }

    if (!confirmPassword) {
      setIsConfirmPasswordHelperText(
        t ? t('typePasswordAgain') : 'Please type in your Password again',
      );
      return false;
    }

    if (password !== confirmPassword) {
      setIsConfirmPasswordHelperText(
        t
          ? t('confirmPasswordMatch')
          : 'Confirm password should match your password',
      );
      return false;
    }

    // password strength

    const payload = {
      firstName,
      lastName,
      userName,
      password,
      active: false,
      date: new Date(),
    };

    props.onSignupClick(payload);

    setBtnDisabled(true);
    setBtnLoading(true);
  };

  useEffect(() => {
    setBtnDisabled(false);
    setBtnLoading(false);
  }, [signupBtnEnabled]);

  return (
    <div className={classes.paper}>
      {headerText ? (
        <Typography component='h1' variant='h6' className={classes.headerText}>
          {headerText}
        </Typography>
      ) : null}
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component='h1' variant='h6'>
        {t ? t('signup') : 'Sign Up'}
      </Typography>
      <Typography component='h6' className={classes.paragraph}>
        {signupText}
      </Typography>
      <form className={classes.form} noValidate>
        <TextField
          variant='outlined'
          margin='normal'
          error={isFirstNameError}
          helperText={isFirstNameHelperText}
          fullWidth
          id='firstName'
          label={t ? t('firstName') : 'First Name'}
          name='firstName'
          autoComplete='firstName'
          onChange={onHandleFirstName}
          autoFocus
        />

        <TextField
          variant='outlined'
          margin='normal'
          error={isLastNameError}
          helperText={isLastNameHelperText}
          fullWidth
          id='lastName'
          label={t ? t('lastName') : 'Last Name'}
          name='lastName'
          autoComplete='lastName'
          onChange={onHandleLastName}
        />

        <TextField
          variant='outlined'
          margin='normal'
          error={isUserNameError}
          helperText={isUserNameHelperText}
          fullWidth
          id='studentUsername'
          label={t ? t('userNameEmail') : 'Username (email)'}
          name='username'
          autoComplete='username'
          onChange={onHandleUserName}
          type='email'
        />

        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant='outlined'
        >
          <InputLabel htmlFor='password'>
            {t ? t('password') : 'Password'}
          </InputLabel>
          <OutlinedInput
            required
            fullWidth
            name='password'
            type={values.showPassword ? 'text' : 'password'}
            id='password'
            autoComplete='current-password'
            onChange={onHandlePassword}
            endAdornment={
              <InputAdornment position='end'>
                <IconButton
                  aria-label='toggle password visibility'
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge='end'
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            aria-describedby='outlined-password-helper-text'
            inputProps={{
              'aria-label': 'password',
            }}
            labelWidth={80}
          />
          <FormHelperText id='outlined-password-helper-text'>
            {isPasswordHelperText}
          </FormHelperText>
        </FormControl>

        <FormControl
          className={clsx(classes.marginConfirm, classes.textField)}
          variant='outlined'
        >
          <InputLabel htmlFor='confirmPassword'>
            {t ? t('confirmPassword') : 'Confirm Password'}
          </InputLabel>
          <OutlinedInput
            required
            fullWidth
            name='confirmPassword'
            type={valuesConfirm.showPassword ? 'text' : 'password'}
            id='confirmPassword'
            autoComplete='confirm-password'
            onChange={onHandleConfirmPassword}
            endAdornment={
              <InputAdornment position='end'>
                <IconButton
                  aria-label='toggle confirm password visibility'
                  onClick={handleClickShowPasswordConfirm}
                  onMouseDown={handleMouseDownPasswordConfirm}
                  edge='end'
                >
                  {valuesConfirm.showPassword ? (
                    <Visibility />
                  ) : (
                    <VisibilityOff />
                  )}
                </IconButton>
              </InputAdornment>
            }
            aria-describedby='outlined-passwordconfirm-helper-text'
            inputProps={{
              'aria-label': 'passwordconfirm',
            }}
            labelWidth={140}
          />
          <FormHelperText id='outlined-passwordconfirm-helper-text'>
            {isConfirmPasswordHelperText}
          </FormHelperText>
        </FormControl>

        <Grid className={classes.btnWrapper}>
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
            onClick={onSignupClick}
            disabled={btnDisabled}
            startIcon={
              btnLoading ? (
                <CircularProgress size='1.0rem' color='primary' />
              ) : null
            }
          >
            {t ? t('signup') : 'Sign Up'}
          </Button>
        </Grid>
        <Grid container>
          <Grid item xs>
            <Link href={`${sName}/signin`} variant='body2'>
              {t ? t('backSignin') : 'Back to Sign In'}
            </Link>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export default SignupContent;

SignupContent.propTypes = {
  schoolName: PropTypes.string,
  signupBtnEnabled: PropTypes.bool,
  setLoading: PropTypes.func,
  onSignupClick: PropTypes.func,
  t: PropTypes.func,
};
